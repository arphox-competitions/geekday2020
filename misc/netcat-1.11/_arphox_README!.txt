Download link:
https://eternallybored.org/misc/netcat/netcat-win32-1.11.zip
(ha nem működne, google: "netcat windows download")

Egy cheat sheet link:
https://www.sans.org/security-resources/sec560/netcat_cheat_sheet_v1.pdf

----------------------------------------------------------------------------------------------------
UDP hallgatózás:

nc -L -u -p 55555 -vv

> NOTE
A listen úgy működik (legalábbis UDP esetben, TCP-nél még nem próbáltam), hogy ha a küldő disconnect-el, akkor leáll a figyeléssel. Ezután enterleütés után kiírja hogy pl. " sent 1, rcvd 545" majd kilép.
A kilépés elkerülhető úgy, hogy a listent nem kicsi l-el, hanem nagy L-el indítjuk (tehát nc -L ...), viszont attól még ha disconnect van ugyanúgy entert le kell ütni ami így elég gagyi megoldás.
Ebből azt kell megtanulni, hogy ha UDP listen-re használod a NC-t, akkor érdemes nagy L-el indítani viszont figyelni kell arra hogy ha épp gyanúsan nem látunk semmi bejövő üzenetet, csapjunk rá az enterre, hátha csak megszakította a küldő...

----------------------------------------------------------------------------------------------------
UDP küldés:
nc -u localhost 55555 -vv

Miután ezt kiadtad, beírod az üzenetet, és enterlenyomáskor küldi el (CRLF-et is elküldi)

----------------------------------------------------------------------------------------------------

General:
-vv: very verbose, kihagyható
-l: [l]isten
-L: listen and restart listenning if finished
-u: use UDP