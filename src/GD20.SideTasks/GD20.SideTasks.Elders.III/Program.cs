﻿using System;

namespace GD20.SideTasks.Elders.III
{
    public class Program
    {
        private static bool _isLogEnabled = false;

        public enum Direction
        {
            Left = 0,
            Right
        }

        public static void Main(string[] args)
        {
            var count = long.Parse(Console.ReadLine());
            var start = long.Parse(Console.ReadLine());
            var direction = (Direction)Enum.Parse(typeof(Direction), Console.ReadLine(), true);

            Log($"{count} {start} {direction}");

            var result = CalculateSurvivor(count, start, direction);
            Console.WriteLine(result);

            if (_isLogEnabled)
                Console.ReadLine();
        }

        public static long CalculateSurvivor(long count, long start, Direction direction)
        {
            long pov = GetNearestBiggerPov(count);

            long diff = pov - count;
            if (diff == 0)
                return start;

            if (direction == Direction.Left)
                return (start - diff + count) % count;

            return (start + diff) % count;
        }

        public static long GetNearestBiggerPov(long count)
        {
            long pov = 2;
            while (pov - count < 0)
                pov *= 2;

            return pov;
        }

        private static void Log(string message)
        {
            if (!_isLogEnabled)
                return;

            Console.WriteLine($"--Log: {message}");
        }
    }
}
