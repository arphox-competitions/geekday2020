﻿using FluentAssertions;
using GD20.SideTasks.Elders.II;
using NUnit.Framework;

namespace GD20.SideTasks.Elders.Tests
{
    [TestFixture]
    public class SideTask2Tests
    {
        [TestCase("[1,2,3,5,a,b,c,d,e,z,A,B,C]", "1-3,5,a-e,z,A-C")]
        [TestCase("[1,a,2,b,3,c]", "1-3,a-c")]
        [TestCase("[A,a,b,A,3,A,B,1,A,C,D,6,5,z,Z,9,9,D]", "1,3,5,6,9,a,b,z,A-D,Z")]
        public void TestCalculateOrderedSequences(string input, string expectedResult)
        {
            string result = Program.CalculateOrderedSequences(input);
            result.Should().Be(expectedResult);
        }
    }
}