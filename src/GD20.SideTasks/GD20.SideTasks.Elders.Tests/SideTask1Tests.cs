﻿using FluentAssertions;
using NUnit.Framework;

namespace GD20.SideTasks.Elders.Tests
{
    [TestFixture]
    public class SideTask1Tests
    {
        [Test]
        public void TestMethod1()
        {
            long[,] dungeon =
            {
                {-2, -3, 3},
                {-5, -10, 1},
                {10, 30, -5}
            };

            var minAmmo = I.Program.CalculateMinAmmo(dungeon);
            minAmmo.Should().Be("7"); // should be 7
        }

        [Test]
        public void TestMethod2()
        {
            long[,] dungeon =
            {
                {-9, 0, 0, 0, -9},
                {-1, 9, -4, 5, -7},
                {-2, 0, 0, -1, -8},
                {-3, 0, 0, -7, 0}
            };

            var minAmmo = I.Program.CalculateMinAmmo(dungeon);
            minAmmo.Should().Be("10");
        }
    }
}
