using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("GD20.SideTasks.Elders.Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Abesse Zrt.")]
[assembly: AssemblyProduct("GD20.SideTasks.Elders.Tests")]
[assembly: AssemblyCopyright("Copyright © Abesse Zrt. 2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("0c8eabc5-4a87-4ade-b17e-914657996c89")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
