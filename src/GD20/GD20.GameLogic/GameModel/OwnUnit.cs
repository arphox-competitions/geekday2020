﻿using System.Collections.Generic;
using System.Linq;

namespace GD20.GameLogic.GameModel
{
    public class OwnUnit : GameItem
    {
        public int Ammo { get; set; }
        public int Hp { get; }

        public OwnUnit(int x, int y, int itemId, ItemType itemType, int ammo, int hp)
            : base(x, y, itemId, itemType)
        {
            Ammo = ammo;
            Hp = hp;
        }

        public IReadOnlyList<GameItem> Targets { get; set; }

        public bool OutOfAmmo => Ammo <= 0;

        public bool HasNoTarget => Targets.Count == 0;

        public override string ToString()
        {
            string targetsInSight = Targets == null ? "?" : Targets.Count.ToString();
            string targetsPart = $"{targetsInSight} targets in sight";
            return $"{ItemType.ToString()} #{ItemId} @({X},{Y}), HP: {Hp}, AMMO: {Ammo}, {targetsPart}";
        }

        public string ToStringDetailed()
        {
            string str = string.Join(",", Targets.Select(t => $"{t.ItemId} @({t.X},{t.Y})"));
            return $"{ToString()}: {str}";
        }
    }
}
