﻿using System;

namespace GD20.GameLogic.GameModel
{
    public sealed class Command
    {
        public OwnUnit Unit { get; }
        public CommandType CommandType { get; }
        public int? TargetId { get; }

        public Command(OwnUnit unit, CommandType commandType, int? targetId = null)
        {
            Unit = unit ?? throw new ArgumentNullException(nameof(unit));
            CommandType = commandType ?? throw new ArgumentNullException(nameof(commandType));
            TargetId = targetId;
        }

        public override string ToString()
        {
            return TargetId != null
                ? $"{Unit.ItemId} {CommandType.RawCommand} to {TargetId}"
                : $"{Unit.ItemId} {CommandType.RawCommand} (cost: {CommandType.Cost})";
        }
    }
}
