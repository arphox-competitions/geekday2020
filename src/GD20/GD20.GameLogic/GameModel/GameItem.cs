﻿using System.Linq;
using System.Numerics;

namespace GD20.GameLogic.GameModel
{
    public class GameItem
    {
        public int X { get; }
        public int Y { get; }
        public ItemType ItemType { get; }
        public int ItemId { get; }

        public GameItem(int x, int y, int itemId, ItemType itemType)
        {
            X = x;
            Y = y;
            ItemId = itemId;
            ItemType = itemType;
        }

        public Vector2 ToVector() => new Vector2(X, Y);

        public override string ToString() => $"{ItemType.ToString()} #{ItemId} @({X},{Y})";

        internal static ItemType GetItemType(int itemId, int[] selfUnitIds)
        {
            if (selfUnitIds.Contains(itemId))
                return ItemType.OwnUnit;

            return itemId switch
            {
                1 => ItemType.Wall,
                2 => ItemType.BonusAmmo,
                3 => ItemType.BonusHealth,
                _ => ItemType.EnemyUnit
            };
        }
    }
}
