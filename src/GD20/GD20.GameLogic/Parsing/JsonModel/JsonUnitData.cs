﻿using Newtonsoft.Json;

namespace GD20.GameLogic.Parsing.JsonModel
{
    public sealed class JsonUnitData
    {
        [JsonProperty("UnitId")]
        public int UnitId { get; set; }

        [JsonProperty("Ammo")]
        public int Ammo { get; set; }

        [JsonProperty("HealthPoint")]
        public int HealthPoint { get; set; }

        [JsonProperty("X")]
        public int X { get; set; }

        [JsonProperty("Y")]
        public int Y { get; set; }

        public override string ToString() => $"Unit #{UnitId} at ({X},{Y}), HP: {HealthPoint}, AMMO: {Ammo}";
    }
}
