﻿using Newtonsoft.Json;

namespace GD20.GameLogic.Parsing.JsonModel
{
    public class JsonResponse
    {
        [JsonProperty("UnitId")]
        public int UnitId { get; set; }

        [JsonProperty("Action")]
        public string Action { get; set; }

        [JsonProperty("TargetId")]
        public string TargetId { get; set; }
    }
}
