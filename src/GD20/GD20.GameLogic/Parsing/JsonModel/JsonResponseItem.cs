﻿using GD20.GameLogic.GameModel;
using Newtonsoft.Json;

namespace GD20.GameLogic.Parsing.JsonModel
{
    public class JsonResponseItem
    {
        [JsonProperty("UnitId")]
        public int UnitId { get; }

        [JsonProperty("Action")]
        public string Action { get; }

        [JsonProperty("TargetId")]
        public int? TargetId { get; }

        public JsonResponseItem(Command command)
        {
            UnitId = command.Unit.ItemId;
            Action = command.CommandType.RawCommand;
            TargetId = command.TargetId;
        }
    }
}
