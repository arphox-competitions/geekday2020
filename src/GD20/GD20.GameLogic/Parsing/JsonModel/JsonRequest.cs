﻿using Newtonsoft.Json;

namespace GD20.GameLogic.Parsing.JsonModel
{
    public sealed class JsonRequest
    {
        [JsonProperty("you_are")]
        public int YouAre { get; set; }

        [JsonProperty("your_units")]
        public int[] YourUnits { get; set; }

        [JsonProperty("your_unitdatas")]
        public JsonUnitData[] YourUnitDatas { get; set; }

        [JsonProperty("you_see")]
        public JsonVisibleItem[] VisibleItems { get; set; }
    }
}
