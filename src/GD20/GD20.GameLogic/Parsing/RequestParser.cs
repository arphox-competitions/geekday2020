﻿using System;
using System.Linq;
using System.Text;
using GD20.Common;
using GD20.GameLogic.GameModel;
using GD20.GameLogic.Parsing.JsonModel;
using Newtonsoft.Json;
using Serilog;

namespace GD20.GameLogic.Parsing
{
    internal static class RequestParser
    {
        internal static MapInfo Parse(byte[] value, ILogger logger)
        {
            logger.Debug("Request started...");

            string asString = Encoding.UTF8.GetString(value);
            JsonRequest jsonRequest = JsonConvert.DeserializeObject<JsonRequest>(asString);
            logger.Debug("JsonConvert.Deserialize done...");

            GameItem[] allItems = jsonRequest.VisibleItems
                .Select(i => new GameItem(i.X, i.Y, i.ItemId, GameItem.GetItemType(i.ItemId, jsonRequest.YourUnits)))
                .ToArray();

            OwnUnit[] ownUnits = jsonRequest.YourUnitDatas.Select(i => new OwnUnit(i.X, i.Y, i.UnitId, ItemType.OwnUnit, i.Ammo, i.HealthPoint)).ToArray();
            GameItem[] enemyUnits = allItems.Where(x => x.ItemType == ItemType.EnemyUnit).DistinctBy(x => x.ItemId).ToArray();
            GameItem[] bonuses = allItems.Where(x => x.ItemType == ItemType.BonusAmmo ||
                                                 x.ItemType == ItemType.BonusHealth).ToArray();

            MapInfo mapInfo = new MapInfo(allItems, ownUnits, enemyUnits, bonuses);

            LogEnemies(mapInfo, logger);

            return mapInfo;
        }

        private static void LogEnemies(MapInfo mapInfo, ILogger logger)
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine("Enemies:");
            str.Append(string.Join(Environment.NewLine, mapInfo.EnemyUnits.Select(x => x.ToString())));
            logger.Information(str.ToString());
        }
    }
}
