﻿using System;

namespace GD20.Common
{
    public static class DateTimeExtensions
    {
        public static string ToMillisecondsTimestamp(this DateTime dateTime) => dateTime.ToString("yyyy-MM-dd HH.mm.ss.fff");
        public static string ToSecondsTimestamp(this DateTime dateTime) => dateTime.ToString("yyyy-MM-dd HH.mm.ss");
    }
}