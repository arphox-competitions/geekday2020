﻿namespace GD20.Common.Settings
{
    public static class GameSettings
    {
        public const int MapSizeX = 1000;
        public const int MapSizeY = 700;

        public const int SightDistance = 150; // set based on 'sight_distance'!
        public const int InitialMovementPoints = 75; // set based on 'initial_move'!
    }
}
