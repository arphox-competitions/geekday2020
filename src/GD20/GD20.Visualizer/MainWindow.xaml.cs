﻿using System.Windows;

namespace GD20.Visualizer
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            new TestWindow1().Show();
            this.Close();
        }
    }
}