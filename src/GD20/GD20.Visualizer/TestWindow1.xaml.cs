﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace GD20.Visualizer
{
    /// <summary>
    /// Interaction logic for TestWindow1.xaml
    /// </summary>
    public partial class TestWindow1 : Window
    {
        private static readonly Random Random = new Random();
        private readonly DispatcherTimer _renderTimer = new DispatcherTimer();
        private byte[] pixels;
        private int width;
        private int height;

        public TestWindow1()
        {
            InitializeComponent();
            ReadImage();

            _renderTimer.Interval = TimeSpan.FromMilliseconds(40);
            _renderTimer.Tick += RenderTimer_Tick;
            _renderTimer.Start();
        }

        private void RenderTimer_Tick(object sender, EventArgs e)
        {
            DrawMap();
        }

        private void ReadImage()
        {
            string[] rawDataRows = File.ReadAllLines(@"C:\Temp\_masks\TR0_v1.txt");

            width = rawDataRows[0].Length;
            height = rawDataRows.Length;

            int[,] matrix = new int[width, height];

            for (int i = 0; i < rawDataRows.Length; i++)
            {
                string row = rawDataRows[i];
                int[] rowValues = row.Select(ch => (int)char.GetNumericValue(ch)).ToArray();
                for (int j = 0; j < rowValues.Length; j++)
                {
                    matrix[j, i] = rowValues[j];
                }
            }

            // Copy to 1D array
            pixels = new byte[height * width * 3];
            int index = 0;
            for (int row = 0; row < height; row++)
            {
                for (int col = 0; col < width; col++)
                {
                    byte color = (byte)(255 * matrix[col, row]);
                    pixels[index++] = color;
                    pixels[index++] = color;
                    pixels[index++] = color;
                }
            }
        }

        private void DrawMap()
        {
            Stopwatch stopper = Stopwatch.StartNew();
            for (int i = 0; i < 1000; i++) // MOD
                pixels[Random.Next(pixels.Length)] += 10;

            WriteableBitmap wbitmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgr24, null); // B G R

            Int32Rect rect = new Int32Rect(0, 0, width, height);
            int stride = 3 * width;
            wbitmap.WritePixels(rect, pixels, stride, 0);

            Image img = new Image();
            img.Stretch = Stretch.None;
            img.Margin = new Thickness(0);

            Maingrid.Children.Clear();
            Maingrid.Children.Add(img);

            img.Source = wbitmap;

            if (DateTime.UtcNow.Ticks % 7 == 0)
                Debug.WriteLine($"Drawed map in {stopper.ElapsedMilliseconds} ms.");
        }
    }
}
