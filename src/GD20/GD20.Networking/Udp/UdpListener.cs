﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using GD20.Networking.Udp.Interfaces;
using Serilog;

namespace GD20.Networking.Udp
{
    /// <summary>
    ///     Non thread-safe UdpListener
    /// </summary>
    /// <remarks>
    ///     Cancellation is not implemented because it wouldn't make sense since this object is not reusable.
    ///     So if you want to cancel, just dispose it.
    /// </remarks>
    public sealed class UdpListener : IDisposable
    {
        private readonly int _port;
        private readonly ILogger _logger;
        private readonly IUdpContextHandler _handler;
        private UdpClient _listener;

        public bool IsDisposed { get; private set; } = false;
        public bool IsRunning { get; private set; } = false;
        public bool IsListening { get; private set; } = false;

        public UdpListener(int port, ILogger logger, IUdpContextHandler handler)
        {
            _port = port;
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _handler = handler ?? throw new ArgumentNullException(nameof(handler));
        }

        public void RunInBackground()
        {
            _logger.Verbose($"{nameof(RunInBackground)} has been called.");
            PrepareRun();
            Task.Factory.StartNew(
                () => EndlessReceive(),
                TaskCreationOptions.LongRunning);
        }

        public void RunSynchronously()
        {
            _logger.Verbose($"{nameof(RunSynchronously)} has been called.");
            PrepareRun();
            EndlessReceive();
        }

        private void EndlessReceive()
        {
            while (true)
            {
                if (IsDisposed)
                {
                    _logger.Information("Stopping next receive cycle as we are disposed.");
                    return;
                }

                ReceiveOnce();
            }
        }

        private void ReceiveOnce()
        {
            byte[] data;
            try
            {
                _logger.Debug("Starting to wait for incoming UDP data...");
                IPEndPoint remoteEndpoint = null; // remote endpoint will be set to this
                IsListening = true;
                data = _listener.Receive(ref remoteEndpoint);
                HandleIncomingData(data, remoteEndpoint);
            }
            catch (SocketException e) when (e.ErrorCode == 10004)
            {
                // 10004: "Interrupted function call. A blocking operation was interrupted by a call to WSACancelBlockingCall."
                // -> this means we have been cancelled
                _logger.Warning("Receive interrupted by a WSACancelBlockingCall.");
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error occured while receiving UDP.");
            }
        }

        private void HandleIncomingData(byte[] data, IPEndPoint remoteEndpoint)
        {
            UdpDatagramContext context = new UdpDatagramContext(remoteEndpoint, data);
            _logger.Information("Received UDP data from {remoteEndpoint}. As UTF8 string: \"{data}\"",
                remoteEndpoint.ToString(),
                Encoding.UTF8.GetString(data));
            _handler.Handle(context);
        }

        private void PrepareRun()
        {
            if (IsDisposed)
                throw new ObjectDisposedException(nameof(UdpListener));

            if (IsRunning)
                throw new InvalidOperationException("The listener is already running.");

            IsRunning = true;
            _listener = new UdpClient(_port, AddressFamily.InterNetwork);
        }

        public void Dispose()
        {
            if (IsDisposed)
                return;

            _listener?.Close();
            _listener?.Dispose();
            _listener = null;
            IsListening = false;
            IsRunning = false;
            IsDisposed = true;
        }
    }
}
