﻿using System;
using System.Linq;
using System.Net;

namespace GD20.Networking.Udp
{
    public readonly struct UdpDatagramContext
    {
        public string RemoteIpAddress { get; }
        public int RemotePort { get; }
        public byte[] Data { get; }
        public DateTime CreatedAt { get; }

        // Simplification ctor
        public UdpDatagramContext(IPEndPoint endpoint, byte[] data)
            : this(endpoint.Address.ToString(), endpoint.Port, data)
        { }

        public UdpDatagramContext(string remoteIpAddress, int remotePort, byte[] data)
        {
            RemoteIpAddress = remoteIpAddress;
            RemotePort = remotePort;
            Data = data;
            CreatedAt = DateTime.UtcNow;
        }

        public override string ToString()
        {
            string sender = $"{RemoteIpAddress}:{RemotePort}";
            string dataBytes = string.Join(',', Data.Select(x => x.ToString()));
            return $"From '{sender}': [{dataBytes}]";
        }
    }
}