﻿using System.Diagnostics;
using GD20.Networking.Udp.Interfaces;
using Serilog;

namespace GD20.Networking.Udp.ContextHandlers
{
    public sealed class ProcessTimeMeasurerUdpContextHandler : UdpContextHandler
    {
        public ProcessTimeMeasurerUdpContextHandler(IUdpContextHandler next, ILogger logger)
            : base(next, logger)
        { }

        public override void Handle(UdpDatagramContext context)
        {
            long start = Stopwatch.GetTimestamp();
            Logger.Verbose("Process time measurer started, calling next handler...");

            Next.Handle(context);
            double elapsedMs = GetElapsedMilliseconds(start, Stopwatch.GetTimestamp());
            Logger.Information("Process time was {ms} ms.", elapsedMs);
        }

        private static double GetElapsedMilliseconds(long start, long stop)
        {
            return (stop - start) * 1000 / (double)Stopwatch.Frequency;
        }
    }
}