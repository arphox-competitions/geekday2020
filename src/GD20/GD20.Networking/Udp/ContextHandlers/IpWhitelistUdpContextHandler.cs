﻿using GD20.Networking.Udp.Interfaces;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GD20.Networking.Udp.ContextHandlers
{
    public sealed class IpWhitelistUdpContextHandler : UdpContextHandler
    {
        private readonly HashSet<string> _enabledIpAddresses;

        public IpWhitelistUdpContextHandler(
            IUdpContextHandler next,
            IReadOnlyCollection<string> enabledIpAddresses,
            ILogger logger)
            : base(next, logger)
        {
            if (enabledIpAddresses is null)
                throw new ArgumentNullException(nameof(enabledIpAddresses));

            _enabledIpAddresses = new HashSet<string>(enabledIpAddresses.Concat(IpHelper.GetAllLocalMachineAddresses()));
        }

        public override void Handle(UdpDatagramContext context)
        {
            string ip = context.RemoteIpAddress;
            if (_enabledIpAddresses.Contains(ip))
            {
                Logger.Verbose("Ip {ip} whitelisted, calling next handler...", ip);
                Next.Handle(context);
            }
            else
            {
                Logger.Information("Ip {ip} NOT on whitelist, NOT calling next handler.", ip);
            }
        }
    }
}
