﻿using GD20.Networking.Udp.Interfaces;
using Serilog;

namespace GD20.Networking.Udp.ContextHandlers
{
    public abstract class UdpContextHandler : IUdpContextHandler
    {
        protected IUdpContextHandler Next { get; }
        protected ILogger Logger { get; }

        public UdpContextHandler(IUdpContextHandler next, ILogger logger)
        {
            Next = next ?? throw new System.ArgumentNullException(nameof(next));
            Logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
        }

        public abstract void Handle(UdpDatagramContext context);
    }
}