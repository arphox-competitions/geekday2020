﻿using System;
using GD20.Networking.Udp.Interfaces;

namespace GD20.Networking.Udp.ContextHandlers
{
    /// <summary>
    ///     Class to make it easier to implement a <see cref="IUdpContextHandler"/>.
    /// </summary>
    public sealed class SimpleUdpContextHandler : IUdpContextHandler
    {
        private readonly Action<UdpDatagramContext> handler;

        public SimpleUdpContextHandler(Action<UdpDatagramContext> handler)
        {
            this.handler = handler ?? throw new ArgumentNullException(nameof(handler));
        }

        public void Handle(UdpDatagramContext context) => handler(context);
    }
}
