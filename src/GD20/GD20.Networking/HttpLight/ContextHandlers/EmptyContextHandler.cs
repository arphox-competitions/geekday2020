﻿using GD20.Networking.HttpLight.Interfaces;
using System.Net;

namespace GD20.Networking.HttpLight.ContextHandlers
{
    public sealed class EmptyContextHandler : IContextHandler
    {
        public void Handle(HttpListenerContext context)
        {
            // does nothing
        }
    }
}