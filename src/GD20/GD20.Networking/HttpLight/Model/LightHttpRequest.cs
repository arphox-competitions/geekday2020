﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Text;

namespace GD20.Networking.HttpLight.Model
{
    public sealed class LightHttpRequest
    {
        public string HttpMethod { get; }
        public NameValueCollection Headers { get; }
        public Uri Url { get; }
        public string RawUrl { get; }
        public NameValueCollection QueryString { get; }
        public IPEndPoint RemoteEndPoint { get; }
        public byte[] BodyBytes { get; }
        public Encoding ContentEncoding { get; }

        public LightHttpRequest(
            string httpMethod,
            NameValueCollection headers,
            Uri url,
            string rawUrl,
            NameValueCollection queryString,
            IPEndPoint remoteEndPoint,
            byte[] bodyBytes,
            Encoding contentEncoding)
        {
            HttpMethod = httpMethod;
            Headers = headers;
            Url = url;
            RawUrl = rawUrl;
            QueryString = queryString;
            RemoteEndPoint = remoteEndPoint;
            BodyBytes = bodyBytes;
            ContentEncoding = contentEncoding;
        }
    }
}