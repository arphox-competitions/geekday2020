﻿using System.Text;

namespace GD20.Networking.HttpLight.Model
{
    public sealed class TextResponse : LightHttpResponse
    {
        public TextResponse(
            int statusCode,
            string statusDescription,
            byte[] bodyBytes,
            Encoding contentEncoding)
            : base(statusCode, statusDescription, bodyBytes, contentEncoding, "text/plain")
        { }

        public static TextResponse CreateUtf8Success(string message)
        {
            return new TextResponse(200, "OK", Encoding.UTF8.GetBytes(message), Encoding.UTF8);
        }
    }
}