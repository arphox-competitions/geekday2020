﻿using System;
using GD20.Common.Settings;
using GD20.GameLogic;
using GD20.Networking.HttpLight;
using GD20.Networking.HttpLight.ContextHandlers;
using GD20.Networking.HttpLight.Model;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace GD20.ConsoleApp
{
    internal class Program
    {
        private static void Main()
        {
            Logger logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                //.MinimumLevel.Override("GD20.Networking.HttpLight.LightweightHttpServer", LogEventLevel.Information)
                //.MinimumLevel.Override("GD20.Networking.HttpLight.ContextHandlers.ResponseTimeMeasurerContextHandler", LogEventLevel.Information)
                //.MinimumLevel.Override("GD20.Networking.HttpLight.ContextHandlers.ExceptionHandlingContextHandler", LogEventLevel.Warning)
                .WriteTo.Async(x => x.Console(
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}",
                    restrictedToMinimumLevel: LogEventLevel.Warning))
                .WriteTo.Async(x => x.File(
                    LogSettings.GeneralLogFilePath,
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}"))
                .WriteTo.Async(x => x.File(
                    LogSettings.InfoPlusLogFilePath,
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}",
                    restrictedToMinimumLevel: LogEventLevel.Information))
                .CreateLogger();

            Commander commander = new Commander(logger.ForContext<Commander>());

            using LightweightHttpServer httpServerLight = new LightweightHttpServerBuilder()
                .DisableConcurrentRequestServing()
                .WithLogger(logger.ForContext<LightweightHttpServer>())
                .Add<ResponseTimeMeasurerContextHandler>()
                .Add<ExceptionHandlingContextHandler>()
                //.Add<IpBlacklistContextHandler>(new object[] { new string[] { "192.168.1.199" } })
                //.Add<IpWhitelistContextHandler>(new object[] { new string[] { "192.168.1.20" } })
                .Add<BusinessLogicContextHandler>((Func<LightHttpRequest, LightHttpResponse>)(rq => commander.Answer(rq)))
                .AddPrefixToListen("http://*:54001/")
                .Build();

            httpServerLight.Start();

            Console.WriteLine("Press any key to exit.");
            while(true)
                Console.ReadKey();
        }
    }
}
