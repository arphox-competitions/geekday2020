using System.Numerics;
using FluentAssertions;
using GD20.GameLogic;
using Xunit;

namespace GD20.UnitTest
{
    public class TargetFinderCanShootTests
    {
        [Fact]
        public void Test1()
        {
            int[,] matrix =
            {
                { 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0 },
            };

            for (int fromX = 0; fromX < matrix.GetLength(0); fromX++)
            {
                for (int fromY = 0; fromY < matrix.GetLength(1); fromY++)
                {
                    Vector2 from = new Vector2(0, 0);
                    for (int toX = 0; toX < matrix.GetLength(0); toX++)
                    {
                        for (int toY = 0; toY < matrix.GetLength(1); toY++)
                        {
                            Vector2 to = new Vector2(matrix.GetLength(0) - 1, matrix.GetLength(1) - 1);
                            TargetFinder.CanShoot(matrix, from, to, 100).Should().BeTrue();
                        }
                    }
                }
            }
        }

        [Fact]
        public void Test2()
        {
            int[,] matrix =
            {
                { 0, 0, 0, },
                { 0, 0, 0, },
                { 0, 0, 0, },
                { 0, 0, 0, },
                { 0, 0, 0, },
            };

            Vector2 from = new Vector2(0, 0);
            Vector2 to = new Vector2(4, 0);
            float dist = Vector2.Distance(from, to);

            int i = 0;
            for (; i < dist; i++)
            {
                TargetFinder.CanShoot(matrix, from, to, i).Should().BeFalse();
                TargetFinder.CanShoot(matrix, to, from, i).Should().BeFalse();
            }

            for (int j = 0; j < 4; j++)
            {
                TargetFinder.CanShoot(matrix, from, to, i++).Should().BeTrue();
                TargetFinder.CanShoot(matrix, to, from, i++).Should().BeTrue();
            }
        }

        [Fact]
        public void Test3()
        {
            int[,] matrix =
            {
                { 0, 0, 0, },
                { 0, 0, 0, },
                { 0, 0, 0, },
                { 0, 0, 0, },
                { 0, 0, 0, },
            };

            Vector2 from = new Vector2(0, 0);
            Vector2 to = new Vector2(4, 2);
            float dist = Vector2.Distance(from, to);

            int i = 0;
            for (; i <= dist; i++)
            {
                TargetFinder.CanShoot(matrix, from, to, i).Should().BeFalse();
                TargetFinder.CanShoot(matrix, to, from, i).Should().BeFalse();
            }

            for (int j = 0; j < 4; j++)
            {
                TargetFinder.CanShoot(matrix, from, to, i++).Should().BeTrue();
                TargetFinder.CanShoot(matrix, to, from, i++).Should().BeTrue();
            }
        }

        [Fact]
        public void Test4()
        {
            int[,] matrix =
            {
                { 0, 0, 0, },
                { 0, 0, 0, },
                { 0, 0, 0, },
                { 0, 0, 0, },
                { 0, 1, 0, },
            };

            Vector2 from = new Vector2(0, 0);
            Vector2 to = new Vector2(4, 2);
            float dist = Vector2.Distance(from, to);

            int i = 0;
            for (; i <= dist; i++)
            {
                TargetFinder.CanShoot(matrix, from, to, i).Should().BeFalse();
                TargetFinder.CanShoot(matrix, to, from, i).Should().BeFalse();
            }

            for (int j = 0; j < 4; j++)
            {
                TargetFinder.CanShoot(matrix, from, to, i++).Should().BeTrue();
                TargetFinder.CanShoot(matrix, to, from, i++).Should().BeTrue();
            }
        }

        [Fact]
        public void Test7()
        {
            int[,] matrix =
            {
                { 0, 0, 0, },
                { 0, 0, 0, },
                { 0, 1, 0, },
                { 0, 0, 0, },
                { 0, 0, 0, },
            };

            Vector2 from = new Vector2(0, 0);
            Vector2 to = new Vector2(4, 2);
            TargetFinder.CanShoot(matrix, from, to, 1000).Should().BeFalse();
            TargetFinder.CanShoot(matrix, to, from, 1000).Should().BeFalse();
        }

        [Fact]
        public void Test8()
        {
            int[,] matrix =
            {
                { 0, 0, 0, },
                { 0, 0, 0, },
                { 0, 1, 0, },
                { 0, 0, 0, },
                { 0, 0, 0, },
            };

            Vector2 from = new Vector2(0, 2);
            Vector2 to = new Vector2(4, 0);
            TargetFinder.CanShoot(matrix, from, to, 1000).Should().BeFalse();
            TargetFinder.CanShoot(matrix, to, from, 1000).Should().BeFalse();
        }

        [Fact]
        public void Test9()
        {
            int[,] matrix =
            {
                { 0, 1, 0, },
                { 0, 1, 0, },
                { 0, 1, 0, },
                { 0, 0, 0, },
                { 0, 1, 0, },
            };

            Vector2 from = new Vector2(3, 0);
            Vector2 to = new Vector2(3, 2);
            TargetFinder.CanShoot(matrix, from, to, 1000).Should().BeTrue();
            TargetFinder.CanShoot(matrix, to, from, 1000).Should().BeTrue();
        }

        [Fact]
        public void Test10()
        {
            int[,] matrix =
            {
                { 0, 0, 0, },
                { 0, 0, 0, },
                { 1, 0, 1, },
                { 0, 0, 0, },
                { 0, 0, 0, },
            };

            Vector2 from = new Vector2(0, 1);
            Vector2 to = new Vector2(4, 1);
            TargetFinder.CanShoot(matrix, from, to, 1000).Should().BeTrue();
            TargetFinder.CanShoot(matrix, to, from, 1000).Should().BeTrue();
        }

        [Fact]
        public void Test11()
        {
            int[,] matrix =
            {
                { 1, 1, 1, 0, },
                { 1, 0, 0, 0, },
                { 1, 0, 0, 0, },
                { 0, 0, 1, 1, },
                { 0, 1, 1, 1, },
            };

            Vector2 from = new Vector2(3, 0);
            Vector2 to = new Vector2(1, 3);
            TargetFinder.CanShoot(matrix, from, to, 1000).Should().BeTrue();
            TargetFinder.CanShoot(matrix, to, from, 1000).Should().BeTrue();
        }

        [Fact]
        public void Test12()
        {
            int[,] matrix =
            {
                { 1, 1, 1, 1, 0, },
                { 1, 0, 0, 0, 0, },
                { 1, 0, 0, 0, 0, },
                { 0, 0, 1, 1, 0, },
                { 0, 1, 1, 1, 0, },
            };

            Vector2 from = new Vector2(3, 0);
            Vector2 to = new Vector2(3, 1);
            TargetFinder.CanShoot(matrix, from, to, 1000).Should().BeTrue();
            TargetFinder.CanShoot(matrix, to, from, 1000).Should().BeTrue();
        }

        [Fact]
        public void Test13()
        {
            int[,] matrix =
            {
                { 1, 1, 1, 1, 0, },
                { 1, 0, 0, 0, 0, },
                { 1, 0, 0, 0, 0, },
                { 0, 0, 1, 0, 0, },
                { 0, 1, 1, 1, 0, },
            };

            Vector2 from = new Vector2(3, 0);

            Vector2 to = new Vector2(3, 3);
            TargetFinder.CanShoot(matrix, from, to, 1000).Should().BeFalse();
            TargetFinder.CanShoot(matrix, to, from, 1000).Should().BeFalse();

            to = new Vector2(3, 4);
            TargetFinder.CanShoot(matrix, from, to, 1000).Should().BeFalse();
            TargetFinder.CanShoot(matrix, to, from, 1000).Should().BeFalse();
        }

        [Fact]
        public void Test14()
        {
            int[,] matrix =
            {
                { 1, 1, 1, 1, 0, },
                { 1, 0, 0, 1, 0, },
                { 1, 0, 0, 0, 0, },
                { 0, 0, 1, 0, 0, },
                { 0, 1, 1, 1, 0, },
            };

            Vector2 to = new Vector2(0, 4);

            Vector2 from = new Vector2(3, 0);
            TargetFinder.CanShoot(matrix, from, to, 1000).Should().BeFalse();
            TargetFinder.CanShoot(matrix, to, from, 1000).Should().BeFalse();

            from = new Vector2(3, 1);
            TargetFinder.CanShoot(matrix, from, to, 1000).Should().BeFalse();
            TargetFinder.CanShoot(matrix, to, from, 1000).Should().BeFalse();

            from = new Vector2(2, 1);
            TargetFinder.CanShoot(matrix, from, to, 1000).Should().BeFalse();
            TargetFinder.CanShoot(matrix, to, from, 1000).Should().BeFalse();

            from = new Vector2(2, 2);
            TargetFinder.CanShoot(matrix, from, to, 1000).Should().BeFalse();
            TargetFinder.CanShoot(matrix, to, from, 1000).Should().BeFalse();
        }

        [Fact]
        public void Test15()
        {
            int[,] matrix =
            {
                { 1, 1, 1, 1, 0, 0, 0, 0, },
                { 1, 0, 1, 1, 0, 0, 0, 0, },
                { 1, 0, 1, 1, 0, 0, 0, 0, },
                { 0, 0, 1, 1, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 1, 0, 0, 0, },
                { 0, 0, 0, 1, 1, 0, 0, 0, },
                { 0, 0, 0, 1, 1, 0, 0, 0, },
                { 0, 0, 0, 1, 1, 0, 0, 0, },
                { 0, 0, 0, 1, 1, 0, 0, 0, },
            };

            Vector2 from = new Vector2(6, 0);
            Vector2 to = new Vector2(3, 5);
            TargetFinder.CanShoot(matrix, from, to, 1000).Should().BeTrue();
            TargetFinder.CanShoot(matrix, to, from, 1000).Should().BeTrue();

            to = new Vector2(3, 6);
            TargetFinder.CanShoot(matrix, from, to, 1000).Should().BeTrue();
            TargetFinder.CanShoot(matrix, to, from, 1000).Should().BeTrue();

            to = new Vector2(3, 7);
            TargetFinder.CanShoot(matrix, from, to, 1000).Should().BeTrue();
            TargetFinder.CanShoot(matrix, to, from, 1000).Should().BeTrue();
        }
    }
}
