﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;

// ReSharper disable FunctionNeverReturns
namespace GD20.TcpClientExample
{
    internal class TcpClientExample
    {
        private const string HOST = "localhost";
        private const int PORT = 55555;

        private static void Main()
        {
            Console.WriteLine($"This is the TCP client, connecting to {HOST}:{PORT}...");

            using TcpClient client = new TcpClient(HOST, PORT);
            NetworkStream stream = client.GetStream();

            int counter = 1;
            while (true)
            {
                string message = $"{counter}{Environment.NewLine}";
                byte[] messageBytes = message.AsUtf8Bytes();
                stream.Write(messageBytes, 0, messageBytes.Length);
                Console.WriteLine($"Sent: '{message}'");
                counter++;
                Thread.Sleep(500);

                if (stream.DataAvailable) // response arrived?
                {
                    int i;
                    byte[] bytes = new byte[1000];
                    if ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        string data = Encoding.UTF8.GetString(bytes, 0, i);
                        data = data.Replace("\r", "CR");
                        data = data.Replace("\n", "LF");
                        Console.WriteLine($"Response: '{data}'");
                    }
                }
            }
        }
    }

    internal static class Extensions
    {
        internal static string AsUtf8String(this byte[] bytes)
            => Encoding.UTF8.GetString(bytes);

        internal static byte[] AsUtf8Bytes(this string str)
            => Encoding.UTF8.GetBytes(str);
    }
}
