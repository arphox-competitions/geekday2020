﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Net.Sockets;
using System.Text;
using System.Threading;

// ReSharper disable FunctionNeverReturns
namespace GD20.UdpMessageSender
{
    internal static class UdpMessageSender
    {
        private const int PORT = 55555;

        private static void Main()
        {
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("This is the UDP sender.");
                Console.ResetColor();

                using UdpClient sender = new UdpClient();
                sender.Connect("127.0.0.1", PORT); // UDP is connectionless, this just creates the socket for sending datagrams

                SendMessages(sender);
            }
        }

        private static void SendMessages(UdpClient sender)
        {
            try
            {
                TrySendMessages(sender);
            }
            catch (SocketException e)
            {
                Debugger.Break();
                Console.WriteLine(e);
            }
            finally
            {
                sender.Close();
            }
        }

        private static void TrySendMessages(UdpClient sender)
        {
            int counter = 0;

            //while (true)
            for (int i = 0; i < 15; i++)
            {
                string message = $"Number {counter} at {DateTime.Now.ToString(CultureInfo.InvariantCulture)}\r\n";
                byte[] bytes = Encoding.UTF8.GetBytes(message);

                Console.Write($"Sending '{message.Replace("\r\n", "CRLF")}'...\t");
                sender.Send(bytes, bytes.Length);
                Console.WriteLine("Sent!");

                counter++;
                Thread.Sleep(200);
            }
        }
    }
}
