﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace GD20.TcpServerExample
{
    internal class TcpServerExample
    {
        private const int PORT = 55555;

        private static void Main()
        {
            Console.WriteLine($"This is the TCP listener on port {PORT}.");

            TcpListener server = new TcpListener(new IPEndPoint(IPAddress.Any, PORT));
            server.Start();

            while (true)
            {
                Console.WriteLine("Waiting for a new client...");
                TcpClient client = server.AcceptTcpClient();
                Console.WriteLine($"Client '{client.Client.RemoteEndPoint}' connected!");

                try
                {
                    NetworkStream stream = client.GetStream();
                    int i;
                    byte[] bytes = new byte[1000];
                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        string dataOriginal = Encoding.UTF8.GetString(bytes, 0, i);
                        string data = dataOriginal;
                        data = data.Replace("\r", "CR");
                        data = data.Replace("\n", "LF");
                        Console.WriteLine("Received message: '{0}'", data);

                        // Send back a response
                        byte[] response = $"I got your message which was: '{dataOriginal}'".AsUtf8Bytes();
                        stream.Write(response, 0, response.Length);
                    }
                }
                catch (IOException exc) when (exc.InnerException is SocketException sockex && sockex.ErrorCode == 10054)
                {
                    Console.WriteLine("Client disconnected!" + Environment.NewLine);
                    client.Dispose();
                }
            }
        }
    }

    internal static class Extensions
    {
        internal static string AsUtf8String(this byte[] bytes)
            => Encoding.UTF8.GetString(bytes);

        internal static byte[] AsUtf8Bytes(this string str)
            => Encoding.UTF8.GetBytes(str);
    }
}
