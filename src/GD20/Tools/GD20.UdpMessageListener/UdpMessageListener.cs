﻿using System;
using System.Text;
using System.Threading.Tasks;
using GD20.Networking.Udp;
using GD20.Networking.Udp.ContextHandlers;
using GD20.Networking.Udp.Interfaces;
using Serilog;

namespace GD20.UdpMessageListener
{
    internal static class UdpMessageListener
    {
        private const int PORT = 55555;
        private const int TIMEOUT_SECONDS = 3000;

        private static void Main()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"This is the UDP listener on port {PORT}.");
            Console.WriteLine($"Starting listening for {TIMEOUT_SECONDS} seconds...");
            Console.ResetColor();

            ILogger logger = CreateConsoleLogger();
            IUdpContextHandler handler = CreateHandler(logger);
            using UdpListener listener = new UdpListener(PORT, logger.ForContext<UdpListener>(), handler);

            Task.Run(async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(TIMEOUT_SECONDS)).ConfigureAwait(false);
                SetForegroundToBlue(() => Console.WriteLine(">>> Disposing now <<<"));
                Console.WriteLine("Press enter to exit...");
                listener.Dispose();
            });

            listener.RunInBackground();
            Console.WriteLine("Press enter anytime to exit...");
            Console.ReadLine();
        }

        private static ILogger CreateConsoleLogger()
        {
            return new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .WriteTo.Console(
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}")
                .CreateLogger();
        }

        private static IUdpContextHandler CreateHandler(ILogger logger)
        {
            var businessLogicHandler = new SimpleUdpContextHandler(args => Console.Write(Encoding.UTF8.GetString(args.Data)));

            var whiteListHandler = new IpWhitelistUdpContextHandler(
                businessLogicHandler,
                new string[] { "192.168.1.169" },
                logger.ForContext<IpWhitelistUdpContextHandler>());

            var timeMeasurer = new ProcessTimeMeasurerUdpContextHandler(
                whiteListHandler,
                logger.ForContext<ProcessTimeMeasurerUdpContextHandler>());

            return timeMeasurer;
        }

        private static void SetForegroundToBlue(Action action)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            action();
            Console.ResetColor();
        }
    }
}