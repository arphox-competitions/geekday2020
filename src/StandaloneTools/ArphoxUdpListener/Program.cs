﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ArphoxUdpListener
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.Title = "UDP listener by. Arphox";
            Console.ForegroundColor = ConsoleColor.Green;
            int port = GetPort(args);

            Console.WriteLine("Press ENTER anytime to exit.");
            Console.WriteLine($"Creating UdpClient for port {port}...");
            using UdpClient client = new UdpClient(port);
            Console.WriteLine("Waiting for incoming data...");
            Console.ResetColor();
            RunInBackground(client);
            Console.ReadLine();
        }

        private static int GetPort(string[] args)
        {
            if (ArgsAreUsable(args))
                return int.Parse(args[0]);

            Console.Write("Port: ");
            return int.Parse(Console.ReadLine());
        }

        private static bool ArgsAreUsable(string[] args)
        {
            bool hasProperInputCount = args?.Length == 1;
            if (!hasProperInputCount)
                return false;

            bool isValidPort = int.TryParse(args[0], out int port);
            return isValidPort;
        }

        public static void RunInBackground(UdpClient client)
        {
            Task.Factory.StartNew(
                () => EndlessReceive(client),
                TaskCreationOptions.LongRunning);
        }

        private static void EndlessReceive(UdpClient client)
        {
            while (true)
            {
                ReceiveOnce(client);
            }
        }

        private static void ReceiveOnce(UdpClient client)
        {
            byte[] data;
            try
            {
                IPEndPoint remoteEndpoint = null; // we don't need this
                data = client.Receive(ref remoteEndpoint);
                Console.Write(Encoding.UTF8.GetString(data));
            }
            catch (SocketException e) when (e.ErrorCode == 10004)
            {
                // 10004: "Interrupted function call. A blocking operation was interrupted by a call to WSACancelBlockingCall."
                // -> this means we have been cancelled
                Console.WriteLine("Receive interrupted by a WSACancelBlockingCall.");
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString(), "Error occured while receiving UDP.");
                return;
            }
        }
    }
}